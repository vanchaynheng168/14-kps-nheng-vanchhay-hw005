import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
            showThread();
    }
    public static void showThread(){

        String greeting= "Hello KSHRD!";
        String star ="";
        String str ="I will try my best to be here at HRD.";
        String subtract= "";
        String download="Downloading";
        String dot="";
        String completed="Completed 100%!";

        char[] arr_greeting = greeting.toCharArray(); //convert to char array
        char[] arr_str=str.toCharArray();
        char[] arr_download=download.toCharArray();
        char[] arr_completed=completed.toCharArray();
        for(int i=0;i<arr_greeting.length;i++)
        {
            System.out.print(arr_greeting[i]); //print one by one
            try {
                Thread.sleep(300);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        System.out.println();
        //assign (*) to star
        for(int j=0;j<36;j++){
            star+="*";
        }
        char[] arr_star=star.toCharArray();
        //print star one by one (*************)
        for(int k=0;k<arr_star.length;k++){
            System.out.print(arr_star[k]);
            try {
                Thread.sleep(300);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        System.out.println();
        for (int m=0;m<arr_str.length;m++){
            System.out.print(arr_str[m]);
            try {
                Thread.sleep(300);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        System.out.println();
        for(int j=0;j<36;j++){
            subtract+="-";
        }
        char[] arr_subtract=subtract.toCharArray();
        for (int n=0;n<arr_subtract.length;n++){
            System.out.print(arr_subtract[n]);
            try {
                Thread.sleep(300);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        System.out.println();
        for (int o=0;o<arr_download.length;o++){
            System.out.print(arr_download[o]);
            try {
                Thread.sleep(10);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        //Assign (.) to dot
        for(int p=0; p<10; p++){
            dot+=".";
        }
        char[] arr_dot=dot.toCharArray();
        for (int k=0;k<arr_dot.length;k++) {
            System.out.print(arr_dot[k]);
            try {
                Thread.sleep(500);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        for (int l=0;l<arr_completed.length;l++){
            System.out.print(arr_completed[l]);
            try {
                Thread.sleep(10);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        System.out.println();
    }
}
